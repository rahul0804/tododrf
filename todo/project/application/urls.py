from django.contrib import admin
from django.urls import path
from .views import Register , login , permission
urlpatterns = [
    path('register/',Register.as_view()),
    path('login/',login.as_view()),
    path('detail/',permission.as_view())
]
