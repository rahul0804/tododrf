from rest_framework import serializers
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    Confirm_password = serializers.CharField(max_length=20, write_only=True, required=True, )

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'Confirm_password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(validated_data['username'])
        user.set_password(validated_data['password'])
        user.save()
        return user

    def validate(self, attrs):
        if attrs['password'] != attrs['Confirm_password']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})
        return attrs

class LoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("username","password")


