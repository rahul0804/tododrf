from django.urls import path

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
from .views import *

urlpatterns = [
    path('create/', view.as_view({"get": "list", "post": "create"})),
    path('update/<int:pk>/', update_delete.as_view({"patch": "update", "delete": "destroy",'get':'retrieve'})),

]
