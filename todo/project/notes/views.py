from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.views import APIView
from .serializers import NoteSerializer
from .models import Notes
from rest_framework.permissions import BasePermission
from rest_framework.response import Response


class IsSuperUser(BasePermission):
    def has_permission(self, request, views):
        if request.user.is_superuser:
            return True


class IsUserCreated(BasePermission):
    def has_permission(self, request, views):
        if request.user.is_superuser:
            return True
        return request.user and request.user == Notes.objects.all().get(id=views.kwargs['pk']).CreatedBy


class view(viewsets.ModelViewSet):
    serializer_class = NoteSerializer
    queryset = Notes.objects.all()


    def list(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return super().list(request, *args, **kwargs)
        else:
            serial = Notes.objects.filter(User=request.user)
            print(request.user.id)
            serializer = NoteSerializer(serial, many=True)
            return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        data = request.data
        if Notes.objects.create(User=request.user, CreatedBy=request.user, UpdatedBy=request.user, Notes=data['Notes']):
            return Response({"Status": "Created"})
        return Response({"Status": "NOT Created"})


class update_delete(viewsets.ModelViewSet):
    serializer_class = NoteSerializer
    queryset = Notes.objects.all()
    permission_classes = (IsUserCreated,)

    def update(self, request, *args, **kwargs):
        if Notes.objects.filter(id=kwargs['pk']).update(Notes=request.data['Notes']):
            return Response({"Status": "Created"})
        return Response({"Status": "Not Created"})

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response({"Status": "Destroyed"})


# class view_ind(viewsets.ModelViewSet):
#     serializer_class = NoteSerializer
#     queryset = Notes.objects.all()
#     permission_classes = (IsUserCreated,)

# class view(viewsets.ModelViewSet):
#     serializer_class = NoteSerializer
#     queryset = Notes.objects.all()
#
#     # permission_classes = (IsUserCreated,)
#
#     def list(self, request, *args, **kwargs):
#         print("fghhgfds")
#         serial = Notes.objects.all()
#         print(serial)
#         if not request.user.is_superuser:
#             print("inside")
#             serial = serial.filter(User=request.user)
#             print("****************")
#         print("outside")
#         serializer = NoteSerializer(serial)
#         print(serializer)
#         return Response(serializer.data)
#


# def create(self, request, *args, **kwargs):
#     dat = request.data
#     print(request)
#     Notes.objects.create(User=1,Createdby=1,Updatedby=1)
#     print(dat)
# final={}
# final['data']=dat
# print(final)
# serial = NoteSerializer(final)
# print(serial)
# return Response(serial.data)


# class view(APIView):
#     def get(self, request, *args, **kwargs):
#         print("List #############################################")
#         # print(IsSuperUser.has_permission(self,request))
#         serial = Notes.objects.all()
#         print(serial)
#         serializer = NoteSerializer(serial)
#         return Response(serializer.data)


class view_ind(viewsets.ModelViewSet):
    serializer_class = NoteSerializer
    queryset = Notes.objects.all()
    permission_classes = (IsUserCreated,)

    def update(self, request, *args, **kwargs):
        if Notes.objects.filter(id=kwargs['pk']).update(Notes=request.data['Notes']):
            return Response({"Status": "Created"})
        return Response({"Status": "Not Created"})

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response({"Status": "Destroyed"})
