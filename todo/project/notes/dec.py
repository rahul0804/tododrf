def dec(func):
    def inner():
        s= func()
        return s+" Rahul"
    return inner
@dec
def hello():
    return "Printed"

hello = dec(hello)


print(hello())

# def decor1(func):
#     def inner():
#         x = func()
#         return x * x
#
#     return inner
#
#
# def decor(func):
#     def inner():
#         x = func()
#         return 2 * x
#
#     return inner
#
#
# @decor1
# @decor
# def num():
#     return 10
#
#
# print(num())