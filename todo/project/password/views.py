from django.shortcuts import render
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import smart_str, force_str, smart_bytes, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.contrib.sites.shortcuts import get_current_site
from django.urls import reverse
from django.contrib.auth.models import User
# Create your views here.
from django.core.mail import EmailMessage
from .serializer import ResetPasswordRequestSerializer, SetNewPasswordSerializer
from django.shortcuts import redirect
from rest_framework import generics
from rest_framework.response import Response
from django.core.mail import send_mail


class RequestPasswordResetEmailView(generics.GenericAPIView):
    serializer_class = ResetPasswordRequestSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        email = request.data['email']

        if User.objects.filter(email=email).exists():
            user = User.objects.get(email=email)
            uidb64 = urlsafe_base64_encode(smart_bytes(user.id))
            print(uidb64)
            token = PasswordResetTokenGenerator().make_token(user)
            print(token)
            current_site = get_current_site(
                request=request).domain
            print(current_site)

            relativeLink = 'password/link/' + uidb64 + '/' + token
            # relativeLink = reverse('api/password_reset_confirm2'
            #                        , args={'uidb64': uidb64, 'token': token}
            #                        )

            absurl = 'http://' + current_site + '/' + relativeLink + '/'
            email_body = 'Hello \n Use link below to reset your password \n' + absurl

            data = {'email_body': email_body,
                    'to_email': user.email,
                    'email_subject': 'Reset your password'}

            if send_mail(
                    data['email_subject'],
                    data['email_body'],
                    'rahul.r@vvdntech.in',
                    [data['to_email']],
                    fail_silently=False,
            ):
                return Response({'success': 'We have sent you a link to reset your password on email'})
            return Response({"failed": "Email Not Sent"})
        return Response({"Status": "Email Not Registered"})


class PasswordTokenCheckAPI(generics.GenericAPIView):
    def get(self, request, uidb64, token):
        try:
            id = smart_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(id=id)

            if not PasswordResetTokenGenerator().check_token(
                    user, token):
                return Response({'error': 'Token is not valid,Please Request a new One'})
            return redirect('http://127.0.0.1:8000/password/passwordset/')
            # return Response({'success': True, 'message': 'Credentials Valid',
            #                  'uidb64': uidb64,
            #                  'token': token})
        except DjangoUnicodeDecodeError as e:
            if not PasswordResetTokenGenerator():
                return Response({'error': 'Token is not valid,Please Request a new One'})


class SetNewPasswordView(generics.GenericAPIView):
    serializer_class = SetNewPasswordSerializer

    def patch(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({
            'success': True,
            'message': 'Password Reset Success'
        })
