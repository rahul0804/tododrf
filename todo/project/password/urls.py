from django.contrib import admin
from django.urls import path
from .views import RequestPasswordResetEmailView ,PasswordTokenCheckAPI,SetNewPasswordView
urlpatterns = [
    path('request-reset-email/', RequestPasswordResetEmailView.as_view(), name='request-reset-email'),
    path('link/<uidb64>/<token>/',PasswordTokenCheckAPI.as_view()),
    path("passwordset/",SetNewPasswordView.as_view()),
]




